local character = {}
local ship = {}
local weapon = {}
local engine = {}
local msg_timer = 0
local tlacitka = require("buttons")
local okno = nil

for i=1,20 do 
  ship[i] = love.graphics.newImage("graphics/ship/"..i..".png")
end
for i=1,5 do 
  engine[i] = love.graphics.newImage("graphics/engines/engine"..i..".png")
end
for i=1,10 do 
  weapon[i] = love.graphics.newImage("graphics/weapons/gun0"..i..".png")
end

local bg = love.graphics.newImage("graphics/bg/darkPurple.png")
bg:setWrap("repeat", "repeat")

function character.update(dt)
  quad = love.graphics.newQuad(0, 0, width, height, 256, 256)
  tlacitka.clear()
  if not okno then
    tlacitka.spawn(width / 2 - 100, height / 2 - 50, language.ships, 1)
    tlacitka.spawn(width / 2 - 100, height / 2, language.engines, 2)
    tlacitka.spawn(width / 2 - 100, height / 2 + 50, language.weapons, 3)
  end
  if okno == 1 then
    for i=1,4 do
      tlacitka.spawn(width/2-400 + 200 * (i - 1), height / 2, 250 * player.ship_lvl, i + (player.ship_lvl-1)*4)
    end
  elseif okno == 2 then
    tlacitka.spawn(width/2-100, height / 2, 325 * tonumber(player.engine_lvl), tonumber(player.engine_lvl))
  elseif okno == 3 then
    tlacitka.spawn(width/2-100, height / 2, 350 * tonumber(player.weapon_lvl), tonumber(player.engine_lvl))
  end
  tlacitka.check()
end

function character.draw()
  love.graphics.draw(bg, quad, 0, 0)
  if okno == 1 then
    for i=1,4 do
      love.graphics.draw(ship[i + (player.ship_lvl-1)*4], width/2-400 + 200 * (i - 1) + 50, height / 2 - 100) 
      love.graphics.print(language.ship_lvl_reqire, width/2 - menuf:getWidth(language.ship_lvl_reqire) / 2,  height / 2 + 150)
    end
  elseif okno == 2 then
    if tonumber(player.engine_lvl) > 0 then
      love.graphics.draw(engine[tonumber(player.engine_lvl)], width/2-engine[tonumber(player.engine_lvl)]:getWidth() / 2, height / 2 - 50)
      love.graphics.print(language.engine_lvl_reqire, width/2 - menuf:getWidth(language.ship_lvl_reqire) / 2,  height / 2 + 150)
    end
  elseif okno == 3 then
    if tonumber(player.weapon_lvl) > 0 then
      love.graphics.draw(weapon[tonumber(player.engine_lvl)], width/2-engine[tonumber(player.engine_lvl)]:getWidth() / 2, height / 2 - 50)
      love.graphics.print(language.weapon_lvl_reqire, width/2 - menuf:getWidth(language.ship_lvl_reqire) / 2,  height / 2 + 150)
    end
  end
  tlacitka.draw()
  love.graphics.print(language.money..player.body, width/2 - menuf:getWidth(language.money..player.body) / 2,  height / 2 - 250)
  if msg then
    love.graphics.setColor(255,0,0)
    love.graphics.print(msg, width/2 - menuf:getWidth(msg) / 2,  height / 2 - 150)
    love.graphics.setColor(255,255,255)
    if msg_timer < 90 then
      msg_timer = msg_timer + 1
    else
      msg_timer = 0
      msg = nil
    end
  end
end
function character.mousepressed(x, y, button)
  if button == "l" then
    if tlacitka.click(x, y) then
      if not okno then
        if player.engine_lvl == 0 then
          msg = language.cant
        elseif player.weapon_lvl == 0 then
          msg = language.cant
        else okno = tlacitka.click(x, y) end
      else
        if okno == 1 then
          if tonumber(player.body) >= 250 * tonumber(player.ship_lvl) then
            player.ship = tlacitka.click(x, y)
            player.IMG = love.graphics.newImage("graphics/ship/"..player.ship..".png")
            player.body = tonumber(player.body) - 250 * player.ship_lvl
            player.life = math.ceil(tonumber(player.ship)/4)
            love.filesystem.write("user.body", player.body)
            love.filesystem.write("user.ship", player.ship)
            msg = language.done
          end
        end
        if okno == 2 then
          if tonumber(player.body) >= 325 * tonumber(player.engine_lvl) then
            player.engine = tlacitka.click(x, y)
            love.filesystem.write("user.engine", player.engine)
            player.body = tonumber(player.body) - 325 * player.engine_lvl
            love.filesystem.write("user.body", player.body)
            msg = language.done
          end
        end
        if okno == 3 then
          if tonumber(player.body) >= 350 * tonumber(player.weapon_lvl) then
            player.weapon = tlacitka.click(x, y)
            love.filesystem.write("user.weapon", player.weapon)
            player.body = tonumber(player.body) - 350 * player.weapon_lvl
            love.filesystem.write("user.body", player.body)
            if tonumber(player.weapon) > 0 then
              player.ammo = player.weapon * 10
            else
              player.ammo = 0
            end
            msg = language.done
          end
        end
      end
    end
  end
end

function character.keypressed(key)
  if key == "escape" then
    if not okno then
      state = 1
    else
      okno = nil
    end
  end
end

return character