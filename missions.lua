local mission = {}

local tlacitka = require("buttons")
local kolo = require("kolo")

local bg = love.graphics.newImage("graphics/bg/darkPurple.png")
bg:setWrap("repeat", "repeat")

chapter = nil
mise = nil
if love.filesystem.exists("user.lvl") then
  lvl = love.filesystem.read("user.lvl")
  chapter_default, mise_default = lvl:match("^(.*)-(.*)")
else
  chapter_default, mise_default = 0, 1
end

timer = 0

function mission.update(dt)
  if chyba then
     if timer > 249 then
       chyba = nil
       timer = 0
     end
    timer = timer + 1
  end
  quad = love.graphics.newQuad(0, 0, width, height, 256, 256)
  if mise == nil then
    tlacitka.clear()
    if chapter == nil then
      for i=0,6 do
        if tonumber(chapter_default) > i then
          hotovo = true
        else
          hotovo = nil
        end
        tlacitka.spawn(width / 2 - 100, height / 2 - 150 + 50 * i, language.chapter .. i, i, hotovo)
      end
    else
      for i=1,5 do
        if tonumber(chapter_default) > chapter or tonumber(mise_default) > i then
          hotovo = true
        else
          hotovo = nil
        end
        tlacitka.spawn(width / 2 - 100, height / 2 - 150 + 50 * i, language.mission .. i, i, hotovo)
      end
    end
    tlacitka.check()
  else
    kolo.update(dt)
  end
end

function mission.draw()
  love.graphics.draw(bg, quad, 0, 0)
  if mise == nil then
    tlacitka.draw()
  else
    kolo.draw()
  end
  if chyba then
    love.graphics.print(chyba, width/2 - menuf:getWidth(chyba)/2, height/2)
  end
end

function mission.keypressed(key)
  if mise == nil then
    if key == "escape" then
      if chapter == nil then
        state = 1
      else
        chapter = nil
      end
    end
  else 
    kolo.keypressed(key)
  end
end

function mission.mousepressed(x, y, button)
  if button == "l" then
    if tlacitka.click(x, y) then
      if chapter == nil then
        if tonumber(chapter_default) >= tlacitka.click(x, y) then
          chapter = tlacitka.click(x, y)
        else
          chyba = language.chapter_low
        end
      else
        if tonumber(mise_default) >= tlacitka.click(x, y) or tonumber(chapter_default) > chapter then 
          mise = tlacitka.click(x, y)
        else
          chyba = language.mise_low
        end
      end
    end
  end
end

return mission
