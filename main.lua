-- tvorba config souboru
if not love.filesystem.exists("user.ship") then  
  love.filesystem.newFile("user.ship")
  love.filesystem.newFile("user.engine")
  love.filesystem.newFile("user.weapon")
  love.filesystem.newFile("user.weapon_lvl")
  love.filesystem.newFile("user.engine_lvl")
  love.filesystem.newFile("user.ship_lvl")
  love.filesystem.write("user.ship", 1)
  love.filesystem.write("user.engine", 0)
  love.filesystem.write("user.weapon", 0)
  love.filesystem.write("user.weapon_lvl", 0)
  love.filesystem.write("user.engine_lvl", 0)
  love.filesystem.write("user.ship_lvl", 1)
end

local toggle = false
ship = love.filesystem.read("user.ship")

-- nacteni config souboru
player = {
    ship = love.filesystem.read("user.ship"),
    engine = love.filesystem.read("user.engine"),
    weapon = love.filesystem.read("user.weapon"),
    IMG = love.graphics.newImage("graphics/ship/"..ship..".png"),
    x = 128,
    y = 128,
    score = 0,
    need = 0,
    ship_lvl = love.filesystem.read("user.ship_lvl"),
    engine_lvl = love.filesystem.read("user.engine_lvl"),
    weapon_lvl = love.filesystem.read("user.weapon_lvl")
  }
  if love.filesystem.exists("user.body") then
    player.body = love.filesystem.read("user.body")
  else
    player.body = 0
  end

-- nastavení engine
gamestate = {require("menu"), require("missions"), require("base"), require("character"), require("setting"), require("quit")}
language = require("text/czech")
state = 1

--audio funkce
do
    -- will hold the currently playing sources
    local sources = {}

    -- check for sources that finished playing and remove them
    -- add to love.update
    function love.audio.update()
        local remove = {}
        for _,s in pairs(sources) do
            if s:isStopped() then
                remove[#remove + 1] = s
            end
        end

        for i,s in ipairs(remove) do
            sources[s] = nil
        end
    end

    -- overwrite love.audio.play to create and register source if needed
    local play = love.audio.play
    function love.audio.play(what, how, loop)
        local src = what
        if type(what) ~= "userdata" or not what:typeOf("Source") then
            src = love.audio.newSource(what, how)
            src:setLooping(loop or false)
        end

        play(src)
        sources[src] = src
        return src
    end

    -- stops a source
    local stop = love.audio.stop
    function love.audio.stop(src)
        if not src then return end
        stop(src)
        sources[src] = nil
    end
end

function love.load()
  bgm = love.audio.play("audio/bg.mp3", "stream", true)
  love.audio.setVolume( 0.1 )
  min_dt = 1/60
  next_time = love.timer.getTime()
  if gamestate[state].load then gamestate[state].load() end
end

function love.update(dt)
  love.audio.update()
  mousex = love.mouse.getX()
  mousey = love.mouse.getY()
  width = love.graphics.getWidth( )
  height = love.graphics.getHeight( )
  next_time = next_time + min_dt
  if gamestate[state].update then gamestate[state].update(dt) end
end

function love.draw()
  love.graphics.print("Current FPS: "..tostring(love.timer.getFPS( )), 10, 10)
  if gamestate[state].draw then gamestate[state].draw() end
  local cur_time = love.timer.getTime()
  if next_time <= cur_time then
    next_time = cur_time
    return
  end
  love.timer.sleep(next_time - cur_time)
end

function love.mousepressed(x, y, button)
  if gamestate[state].mousepressed then gamestate[state].mousepressed(x, y, button) end
end

function love.keypressed(key)
  if key == "f11" then
  toggle = not toggle
	love.window.setFullscreen(toggle, "desktop")
  end
  if gamestate[state].keypressed then gamestate[state].keypressed(key) end
end

function love.textinput(t)
  if gamestate[state].textinput then gamestate[state].textinput(t) end
end
