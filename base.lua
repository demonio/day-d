local base = {}
local ship = love.graphics.newImage("graphics/ship/20.png")
local engine = love.graphics.newImage("graphics/engines/engine5.png")
local weapon = love.graphics.newImage("graphics/weapons/gun010.png")
local bg = love.graphics.newImage("graphics/bg/black.png")
bg:setWrap("repeat", "repeat")
local tlacitka = require("buttons")
local msg_timer = 0
function base.update(dt)
  tlacitka.clear()
  quad = love.graphics.newQuad(0, 0, width, height, 256, 256)
  if tonumber(player.ship_lvl) < 5 then
    tlacitka.spawn(width/2- 300, height / 2 - 50, 300 * player.ship_lvl, 1)
  end
  if tonumber(player.engine_lvl) < 5 then
    tlacitka.spawn(width/2+ 100, height / 2 - 50, 400 * player.engine_lvl, 2)
  end
  if tonumber(player.weapon_lvl) < 10 then
    tlacitka.spawn(width/2+ 100, height / 2 + 200, 500 * player.weapon_lvl, 3)
  end
  if tonumber(player.weapon_lvl) < 100 then
    tlacitka.spawn(width/2- 300, height / 2 + 200, 10000, 4)
  end
  tlacitka.check()
end

function base.draw()
  love.graphics.draw(bg, quad, 0, 0)
  
  love.graphics.print(language.ship_building, width/2 - 200 - menuf:getWidth(language.ship_building)/2, height / 2 - 100)
  love.graphics.print(language.lvl..player.ship_lvl, width/2 - 200 - menuf:getWidth(language.lvl..player.ship_lvl)/2, height / 2 - 75)
  
  love.graphics.print(language.engine_building, width/2 + 200 - menuf:getWidth(language.engine_building)/2, height / 2 - 100)
  love.graphics.print(language.lvl..player.engine_lvl, width/2 + 200 - menuf:getWidth(language.lvl..player.engine_lvl)/2, height / 2 - 75)
  
  love.graphics.print(language.weapon_building, width/2 + 200 - menuf:getWidth(language.weapon_building)/2, height / 2 + 150)
  love.graphics.print(language.lvl..player.weapon_lvl, width/2 + 200 - menuf:getWidth(language.lvl..player.weapon_lvl)/2, height / 2 + 175)
  
  love.graphics.print(language.special, width/2 - 200 - menuf:getWidth(language.special)/2, height / 2 + 150)
  love.graphics.print(language.lvl..0, width/2 - 200 - menuf:getWidth(language.lvl..0)/2, height / 2 + 175)
  tlacitka.draw()
  love.graphics.print(language.money..player.body, width/2 - menuf:getWidth(language.money..player.body) / 2,  height / 2 - 250)
  love.graphics.draw(ship, width/2 - 200 - ship:getWidth() / 2, height / 2 - 200)
  love.graphics.draw(engine, width/2 + 200 - engine:getWidth() / 2, height / 2 - 150)
  love.graphics.draw(weapon, width/2 + 200 - weapon:getWidth() / 2, height / 2 + 80)
  love.graphics.setColor(255,0,0)
  love.graphics.draw(weapon, width/2 - 200 - weapon:getWidth() / 2, height / 2 + 80)
  love.graphics.setColor(255,255,255)
  if msg then
    love.graphics.setColor(255,0,0)
    love.graphics.print(msg, width/2 - menuf:getWidth(msg) / 2,  height / 2 - 150)
    love.graphics.setColor(255,255,255)
    if msg_timer < 90 then
      msg_timer = msg_timer + 1
    else
      msg_timer = 0
      msg = nil
    end
  end
end

function base.keypressed(key)
  if key == "escape" then
    state = 1
  end
end

function base.mousepressed(x, y, button)
  if button == "l" then
    if tlacitka.click(x, y) then
      if tlacitka.click(x, y) == 1 then
        if tonumber(player.body) >= 300 * tonumber(player.ship_lvl) then
            player.ship_lvl = player.ship_lvl + 1
            love.filesystem.write("user.ship_lvl", player.ship_lvl)
            player.body = tonumber(player.body) - 300 * (player.ship_lvl - 1)
            love.filesystem.write("user.body", player.body)
            msg = language.done
        else
          msg = language.nomoney
        end
      elseif tlacitka.click(x, y) == 2 then
        if tonumber(player.body) >= 400 * tonumber(player.engine_lvl) then
            player.engine_lvl = player.engine_lvl + 1
            love.filesystem.write("user.engine_lvl", player.engine_lvl)
            player.body = tonumber(player.body) - 400 * (player.engine_lvl - 1)
            love.filesystem.write("user.body", player.body)
            msg = language.done
        else
          msg = language.nomoney
        end
      elseif tlacitka.click(x, y) == 3 then
        if tonumber(player.body) >= 500 * tonumber(player.weapon_lvl) then
            player.weapon_lvl = player.weapon_lvl + 1
            love.filesystem.write("user.weapon_lvl", player.weapon_lvl)
            player.body = tonumber(player.body) - 500 * (player.weapon_lvl - 1)
            love.filesystem.write("user.body", player.body)
            msg = language.done
        else
          msg = language.nomoney
        end
      elseif tlacitka.click(x, y) == 4 then
        if tonumber(player.body) >= 10000 then
            player.weapon = 99999
            love.filesystem.write("user.weapon", player.weapon)
            player.weapon_lvl = player.weapon_lvl + 100
            love.filesystem.write("user.weapon_lvl", player.weapon_lvl)
            player.body = tonumber(player.body) - 10000
            love.filesystem.write("user.body", player.body)
            msg = language.done
            if tonumber(player.weapon) > 0 then
              player.ammo = player.weapon * 10
            else
              player.ammo = 0
            end
        else
          msg = language.nomoney
        end
      end
    end
  end
end

return base