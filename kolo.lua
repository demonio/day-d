local mission = {}

konec = false
pause = false

local bg = love.graphics.newImage("graphics/bg/darkPurple.png")
bg:setWrap("repeat", "repeat")
laser_audio = love.audio.newSource("audio/laser.ogg", "static")
if tonumber(player.weapon) > 0 then
  player.ammo = player.weapon * 10
else
  player.ammo = 0
end
player.life = math.ceil(tonumber(player.ship)/4)

local shoot = {}
local enemy = {}
local boost = {}
local boost_tmp = 15
local meteor = {}
local meteor_tmp = 8
local meteor_dt = 0
local shoot_enemy = {}
local enemy_tmp = 2
local enemy_dt = 0

function mission.update(dt)
  player.need = chapter * 300 + mise * mise * 10
  quad = love.graphics.newQuad(0, 0, width, height, 256, 256)
  if not konec then
  if not pause then
  
    if player.need <= player.score then
      msg = language.won
      konec = true
      chapter_default, mise_default = chapter, mise
      -- TODO pridávání medailí a nastavování globálního skore
      if love.filesystem.exists("user.body") then
        body = love.filesystem.read("user.body")
        love.filesystem.write("user.body", player.score + body)
        player.body = player.score + body
      else
        love.filesystem.newFile("user.body")
        love.filesystem.write("user.body", player.score)
      end
      if love.filesystem.exists("user.lvl") then
        lvl = love.filesystem.read("user.lvl")
        chapter2, mise2 = lvl:match("^(.*)-(.*)")
        if tonumber(chapter2) == chapter and tonumber(mise2) == mise then
          if mise == 5 then
            chapter = chapter + 1
            mise = 1
          else
            mise = mise + 1
          end
          love.filesystem.write("user.lvl", chapter .. "-" .. mise)
          chapter_default, mise_default = chapter, mise
        end
      else
        love.filesystem.newFile("user.lvl")
        if mise == 5 then
          chapter = chapter + 1
          mise = 1
        else
          mise = mise + 1
        end
        love.filesystem.write("user.lvl", chapter .. "-" .. mise)
        chapter_default, mise_default = chapter, mise
      end
    end
    
    if player.life < 1 then
      msg = language.lose
      konec = true
    end
    
    if player.score == boost_tmp then
      make_boost()
      boost_tmp = boost_tmp * 4
    end
    
    if love.keyboard.isDown("up") and player.y > -1 then
      player.y = player.y - 6 - player.engine / 2
    end 
    if love.keyboard.isDown("down") and player.y + player.IMG:getWidth() < height then
      player.y = player.y + 6 + player.engine / 2
    end

    if love.keyboard.isDown("left") and player.x - player.IMG:getHeight() > -1 then
      player.x = player.x - 6 - player.engine / 2
    end 
    if love.keyboard.isDown("right") and player.x < width then
      player.x = player.x + 6 + player.engine / 2
    end
    for i, v in ipairs(shoot) do
      v.x = v.x + 20
      if v.y > width then table.remove(shoot, i) end
    end
    
    --for i, v in 
    for i, v in ipairs(enemy) do		
      v.x = v.x - 3 
      if v.x < - v.IMG:getWidth() then 
        table.remove(enemy, i)
        player.life = player.life - 1 
      end
    end
    
    for i, v in ipairs(meteor) do		
      v.x = v.x - 2
      if v.x < - v.IMG:getWidth() then 
        table.remove(meteor, i)
      end
    end
    
    if player.score > 250 then
      meteor_tmp = 6
    end
    
    for i, v in ipairs(boost) do		
      v.x = v.x - 9
      if v.x < - v.IMG:getWidth() then 
        table.remove(boost, i)
      end
    end
    
    if enemy_dt < enemy_tmp then 
      enemy_dt = enemy_dt + dt
    else
      enemy_dt = 0 
      make_enemy()
      if player.score > 249 then
        meteor_tmp = 7
      end
      if meteor_tmp > meteor_dt then
        meteor_dt = meteor_dt + 1
      else
        if player.score > 99 then
          make_meteor()
        end
        meteor_dt = 0
      end
    end
    
    for i, v in ipairs(shoot) do
      for k, z in ipairs(enemy) do
        if v.x > z.x and v.x < z.x + z.IMG:getWidth() and v.y > z.y and v.y < z.y + z.IMG:getHeight() then
          player.score = player.score + 1
          table.remove(shoot, i)
          table.remove(enemy, k)
          if enemy_tmp > 0.25 then
            enemy_tmp = enemy_tmp - 0.05
          end
        end
      end
    end
    for k, z in ipairs(meteor) do
      if player.x + player.IMG:getWidth()/2 > z.x and player.x + player.IMG:getWidth()/2 < z.x + z.IMG:getWidth() and player.y + player.IMG:getHeight()/2 > z.y and player.y + player.IMG:getHeight()/2 < z.y + z.IMG:getHeight() then
        player.life = player.life - 1
        table.remove(meteor, k)
      end
    end
    for k, z in ipairs(boost) do
      if player.x < z.x and player.x + player.IMG:getWidth() > z.x and player.y < z.y and player.y + player.IMG:getHeight() > z.y then
        if id < 4 then
          player.ammo = player.ammo + id * 10
        else
          player.life = player.life + id - 3
        end
        table.remove(boost, k)
      end
    end
  end end
end

function mission.draw()
  love.graphics.draw(bg, quad, 0, 0)
  love.graphics.draw(player.IMG, player.x, player.y, math.pi/2)
	love.graphics.print(language.score .. player.score, 10, 10)
  love.graphics.print(language.need .. player.need, width / 2 - 100, 10 )
	love.graphics.print(language.life .. player.life, width - 110, 10)
  love.graphics.print(language.ammo .. player.ammo, 10, height - 30)
 
	for i, v in ipairs(boost) do
		love.graphics.draw(v.IMG, v.x, v.y) 
	end
 
	for i, v in ipairs(meteor) do
		love.graphics.draw(v.IMG, v.x, v.y) 
	end

	for i, v in ipairs(enemy) do
		love.graphics.draw(v.IMG, v.x, v.y) 
	end	

	for i, v in ipairs(shoot) do
		love.graphics.draw(v.IMG, v.x, v.y, math.pi/2) 
	end
  
  if konec then
    love.graphics.print(msg, width/2 - menuf:getWidth(msg) / 2, height/2)
    love.graphics.print(language.exit, width/2 - menuf:getWidth(language.exit) / 2, height/2 - 5 - menuf:getHeight())
  end 
  if pause then 
    love.graphics.print(language.pause, width/2 - menuf:getWidth(language.pause) / 2, height/2)
  end
end

function make_shoot(x, y)
	table.insert(shoot, {x = x, y = y + (player.IMG:getWidth()/2), IMG = love.graphics.newImage("graphics/laser/1.png")})
end

function make_boost()
  math.randomseed(os.time())
  id = math.random(1,6)
	IMG = love.graphics.newImage("graphics/boost/"..id..".png")	
	table.insert(boost, {x = width + (IMG:getWidth()), y = math.random(0 , (height - IMG:getHeight())) , IMG = IMG, id = id})
end

function make_enemy()
  if enemy_tmp > 0.25 then
		math.randomseed(os.time())
	end
	IMG = love.graphics.newImage("graphics/enemy/"..math.random(1,4)..".png")	
	table.insert(enemy, {x = width + (IMG:getWidth()), y = math.random(0 , (height - IMG:getHeight())) , IMG = IMG})
end

function make_meteor()
	IMG = love.graphics.newImage("graphics/meteor/"..math.random(1,8)..".png")	
	table.insert(meteor, {x = width + (IMG:getWidth()), y = math.random(0 , (height - IMG:getHeight())) , IMG = IMG})
end


function mission.keypressed(key)
  if key == "escape" then
    chapter = nil
    mise = nil
  end
  if key == " " then
    make_shoot(player.x, player.y)
    laser_audio:play()
  end
  if key == "x" and player.ammo > 0 then
      player.ammo = player.ammo - 1
      make_shoot(player.x, player.y - 25)
      make_shoot(player.x, player.y + 25)
      laser_audio:play()
  end
  if key == "p" then
    pause = not pause
  end
  if konec and key == "return" then
    chapter = nil
    mise = nil
    if tonumber(player.weapon) > 0 then
      player.ammo = player.weapon * 10
    else
      player.ammo = 0
    end
    player.life = math.ceil(tonumber(player.ship)/4)
    state = 1
    player.score = 0
    konec = false
    shoot = {}
    enemy = {}
    boost = {}
    boost_tmp = 15
    meteor = {}
    meteor_tmp = 8
    meteor_dt = 0
    shoot_enemy = {}
    enemy_tmp = 2
    enemy_dt = 0
  end
end

return mission
