-- Proměne ktere potřebujeme na generovaní tlačítek
local tlacitka = {}
local button = {}
local buttonbg = love.graphics.newImage("graphics/images/ui/button_bg.png")
local buttonbg_active = love.graphics.newImage("graphics/images/ui/button_bg_active.png")
local medal = love.graphics.newImage("graphics/medals/1.png")
local buttonbg_x = (buttonbg:getWidth( ) / 2) - 2
local buttonbg_y = (buttonbg:getHeight( ) / 2) - 2

-- Vytvoření tlačítka
function tlacitka.spawn(x, y, text, value, hotovo)
	table.insert(button, {x = x, y = y, text = text, mouseover = false, value = value, hotovo = hotovo})
end

-- Vykreslení tlačítka
function tlacitka.draw()
	for i, v in ipairs(button) do
		if v.mouseover == true then
			love.graphics.draw(buttonbg_active, v.x, v.y)
			
		else
			love.graphics.draw(buttonbg, v.x, v.y)
			
		end
    if v.hotovo then
      love.graphics.draw(medal, v.x + 220, v.y)
      love.graphics.draw(medal, v.x - 50, v.y)
      love.graphics.setColor(0, 255, 0)
    end
		love.graphics.print( v.text, v.x + buttonbg_x - menuf:getWidth(v.text) / 2, v.y + buttonbg_y - menuf:getHeight() / 2)
		love.graphics.setColor(255, 255, 255)
	end
end

-- Testovaní jestli je tlačítko zmačknuté
function tlacitka.click(x, y)
	for i, v in ipairs(button) do
		if mousex > v.x and mousex < v.x + buttonbg_x * 2 and mousey > v.y and mousey < v.y + buttonbg_y * 2 then
			return v.value
		end
	end
end

-- Testovaní najetí myší
function tlacitka.check()
	for i, v in ipairs(button) do
		if mousex > v.x and mousex < v.x + buttonbg_x * 2 and mousey > v.y and mousey < v.y + buttonbg_y * 2 then
			v.mouseover = true
		else
			v.mouseover = false
		end
	end
end

-- Smazaní všech hlavních tlačítek
function tlacitka.clear()
	for k in pairs (button) do
		button [k] = nil
	end
end

return tlacitka
