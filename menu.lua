local menu = {}
local tlacitka = require("buttons")

menuf = love.graphics.newFont("graphics/fonts/menu.ttf", 20)
love.graphics.setFont( menuf )
local kenney = love.graphics.newImage("graphics/images/kenney.png")
local venca = love.graphics.newImage("credits.png")

local bg = love.graphics.newImage("graphics/bg/darkPurple.png")
bg:setWrap("repeat", "repeat")

function menu.update(dt)
  quad = love.graphics.newQuad(0, 0, width, height, 256, 256)
  tlacitka.clear()
  tlacitka.spawn(width / 2 - 100, height / 2 - 100, language.missions, 2)
  tlacitka.spawn(width / 2 - 100, height / 2 - 50, language.base, 3)
  tlacitka.spawn(width / 2 - 100, height / 2, language.character, 4)
  tlacitka.spawn(width / 2 - 100, height / 2 + 50, language.setting, 5)
  tlacitka.spawn(width / 2 - 100, height / 2 + 100, language.quit, 6)
  tlacitka.check()
end

function menu.draw()
  love.graphics.draw(bg, quad, 0, 0)
  tlacitka.draw()
  love.graphics.draw(venca, 10, height - venca:getHeight()- 10)
  love.graphics.draw(kenney, width - kenney:getWidth(), height - kenney:getHeight())
end

function menu.mousepressed(x, y, button)
  if button == "l" then
    if tlacitka.click(x, y) then
      state = tlacitka.click(x, y)
    end
  end
end

function menu.keypressed(key)
  if key == "escape" then
    love.event.quit()
  end
end

return menu