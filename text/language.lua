local language = {}

language.missions = "Missions"
language.base = "Base"
language.character = "Ship"
language.setting = "Settings"
language.quit = "Leave"
language.fullscreen = "Fullscrean"
language.need_player = "Generate player at first"
language.chapter = "Chapter"
language.mission = "Mission"
language.score = "Points "
language.life = "Lives "
language.need = "Minimal score"
language.won = "Mission accomplished"
language.lose = "You lose"
language.exit = "Press return for end"
language.chapter_low = "First, complete the previous chapter"
language.mise_low = "First, complete the previous mission"
language.ammo = "Special ammunition(Key X) "
language.pause = "Pause"
language.weapons = "Weapons"
language.engines = "Engines"
language.ships = "Ships"
language.ship_lvl_reqire = "For better ships Improve base"
language.money = "Shopping point: "
language.done = "Purchase complete"
language.cant = "Improve base before continue"
language.engine_lvl_reqire = "For better engines Improve base"
language.weapon_lvl_reqire = "For better weapons Improve base"
language.special = "Secret weapon"
language.lvl = "LVL: "
language.ship_building = "Hangar"
language.engine_building = "Workshop"
language.weapon_building = "Armory"
language.nomoney = "Need more points"

return language