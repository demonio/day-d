local setting = {}
local click = nil
tlacitka = require("buttons")

local toggle = false

bg = love.graphics.newImage("graphics/bg/darkPurple.png")
bg:setWrap("repeat", "repeat")

function screen_toggle()
  toggle = not toggle
	love.window.setFullscreen(toggle, "desktop")
  click = nil
end

function setting.update(dt)
  quad = love.graphics.newQuad(0, 0, width, height, 256, 256)
  tlacitka.clear()
  tlacitka.spawn(width / 2 - 100, height / 2, language.fullscreen, "screen")
  tlacitka.check()
  if click == "screen" then screen_toggle() end
end

function setting.draw()
  love.graphics.draw(bg, quad, 0, 0)
  tlacitka.draw()
end

function setting.mousepressed(x, y, button)
  if button == "l" then
    click = tlacitka.click(x, y)
  end
end

function setting.keypressed(key)
  if key == "escape" then
    state = 1
  end
end


return setting